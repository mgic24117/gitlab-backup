#!/bin/env python3
# system modules
import os
import argparse
import itertools
import subprocess
import logging
import logging.config
import urllib
import pathlib
import contextlib
import shlex
import re

# external modules
import gitlab
import rich
from rich.logging import RichHandler
from rich.console import Console
from rich.syntax import Syntax
from rich.panel import Panel
from rich.progress import Progress, MofNCompleteColumn

logging.config.dictConfig(dict(version=1, disable_existing_loggers=True))
logger = logging.getLogger(__name__)

console = Console(stderr=True)
parser = argparse.ArgumentParser(description="GitLab recursive Backup")
parser.add_argument(
    "--gitlab", help="GitLab instance URL", default="https://gitlab.com"
)
parser.add_argument(
    "--token",
    help="GitLab token (default: GITLAB_TOKEN envvar)",
    default=os.environ.get("GITLAB_TOKEN"),
)
parser.add_argument(
    "--iter", help="Don't build project list up front. (faster)", action="store_true"
)
parser.add_argument(
    "-o", "--outdir", help="Output directory", type=pathlib.Path, default="."
)
parser.add_argument("--group", help="Name of GitLab group to back up", required=True)
parser.add_argument("-v", "--verbose", help="more output", action="count", default=0)
parser.add_argument("-q", "--quiet", help="less output", action="count", default=0)


@contextlib.contextmanager
def noop():
    yield


def run(func, cmd, **kwargs):
    logger.info(f"🚀 Running {cmd}")
    if logger.getEffectiveLevel() < logging.INFO:
        console.print(
            Syntax(
                f"""
# 📋 Copy-Pasteable

{shlex.join(cmd)}
""",
                "bash",
                word_wrap=True,
            )
        )
    return func(cmd, **{**dict(encoding="utf-8", errors="ignore"), **kwargs})


def git_backup(url, path):
    path = pathlib.Path(path)
    if path.exists():
        logger.info(f"Updating existing repo {url} in {str(path)!r}")
        run(
            subprocess.run, ["git", "-C", str(path), "remote", "set-url", "origin", url]
        )
        run(subprocess.run, ["git", "-C", str(path), "fetch", "--all"])
    else:
        result = run(
            subprocess.run,
            ["git", "clone", url, str(path)],
        )
        if result.returncode:
            logger.info(f"Command {result.args} exited with code {result.returncode}")
            if result.stdout:
                console.print(
                    Panel(
                        Syntax(result.stdout, "txt"),
                        title=f"{shlex.join(result.args)}",
                        subtitle="stdout",
                    )
                )
            if result.stderr:
                console.print(
                    Panel(
                        Syntax(result.stdout, "txt"),
                        title=f"{shlex.join(result.args)}",
                        subtitle="stderr",
                    )
                )


def cli():
    args = parser.parse_args()
    logging.basicConfig(
        level=logging.INFO - (args.verbose - args.quiet) * 10,
        format="%(message)s",
        datefmt="[%X]",
        handlers=[RichHandler(console=console)],
    )

    gl = gitlab.Gitlab(args.gitlab, args.token)

    def gitlab_projects(group):
        if not hasattr(group, "projects"):
            logger.debug(f"ℹ️   Retrieving info for group {group.name!r}")
            group = gl.groups.get(group.id)
        logger.debug(f"🔎  Finding projects of group {group.name!r}")
        projects = group.projects.list(all=True)
        logger.info(f"Found {len(projects)} projects in group {group.name!r}")
        yield from projects
        logger.debug(f"🔎  Finding subgroups of group {group.name!r}...")
        subgroups = group.subgroups.list(all=True)
        logger.info(f"Found {len(subgroups)} subgroups in group {group.name!r}")
        for subgroup in subgroups:
            yield from gitlab_projects(subgroup)

    group = gl.groups.get(args.group)

    projects = gitlab_projects(group)
    if not args.iter:
        msg = "(skip this with --iter)"
        projects_ = []
        with (
            console.status(f"🔎  Finding projects in group {args.group!r}... {msg}")
            if logger.getEffectiveLevel() <= logging.INFO
            else noop()
        ) as status:
            for n, project in enumerate(projects, start=1):
                if status:
                    status.update(
                        f"🔎  Finding projects in group {args.group!r} ({n}/?)... {msg}"
                    )
                projects_.append(project)
        projects = projects_

    if args.outdir.exists():
        logger.warning(
            f"Output directory {str(args.outdir)!r} exists, will update clones in it"
        )
    else:
        logger.info(f"Creating output directory {str(args.outdir)!r}")
        os.makedirs(str(args.outdir))

    columns = list(Progress.get_default_columns())
    columns.insert(1, MofNCompleteColumn())
    with Progress(*columns, console=console) as progress:

        task_projects = progress.add_task(
            f"Backing up projects",
            total=len(projects) if hasattr(projects, "__len__") else None,
        )
        for n, project in enumerate(
            progress.track(projects, task_id=task_projects), start=1
        ):
            # with console.status(f"ℹ️   Retrieving info for project {project.name!r}"):
            #     project = gl.projects.get(project.id)
            outrepopath = args.outdir / urllib.parse.urlparse(project.web_url).path[1:]
            logger.info(
                f"Back up project #{n} {project.web_url} to {str(outrepopath)!r}"
            )
            git_backup(project.ssh_url_to_repo, outrepopath)
            if project.wiki_enabled:
                wiki_url = re.sub(r"(\.git)$", r".wiki\1", project.ssh_url_to_repo)
                wiki_repopath = str(outrepopath) + ".wiki"
                logger.info(
                    f"Back up project #{n} wiki {wiki_url} to {str(outrepopath)!r}"
                )
                git_backup(wiki_url, wiki_repopath)


if __name__ == "__main__":
    cli()
