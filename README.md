# Back Up Your GitLab Group's Repos Including Wikis

Usage:

```python
# get the project
git clone https://gitlab.com/tue-umphy/software/gitlab-backup
cd gitlab-backup
# install dependencies
poetry install
poetry shell
# set GitLab token interactively (generate one on GitLab)
read GITLAB_TOKEN
exprort GITLAB_TOKEN
# alternatively put something like   GITLAB_TOKEN=glpat-AAAAAAAAAAAAAAAAAA-A  into token.sh, then
source token.sh
# Run the backup
python gitlab-backup.py --group YOURGROUP
```

Help page:

```
> python gitlab_backup.py -h
usage: gitlab_backup.py [-h] [--gitlab GITLAB] [--token TOKEN] [--iter] [-o OUTDIR] --group GROUP [-v] [-q]

GitLab recursive Backup

options:
  -h, --help            show this help message and exit
  --gitlab GITLAB       GitLab instance URL
  --token TOKEN         GitLab token (default: GITLAB_TOKEN envvar)
  --iter                Don't build project list up front. (faster)
  -o OUTDIR, --outdir OUTDIR
                        Output directory
  --group GROUP         Name of GitLab group to back up
  -v, --verbose         more output
  -q, --quiet           less output
```
